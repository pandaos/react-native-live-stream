/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import React, {Component} from 'react';
import ReactNative, {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    requireNativeComponent,
    NativeModules,
    DeviceEventEmitter,
    TouchableHighlight
} from 'react-native';

var LiveVideoView = requireNativeComponent('RNLiveVideoView', null);

export class LiveVideo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'LFLiveReady'
        }
    }


    componentWillMount() {
        NativeModules.RNLiveVideoViewManager.stopStreaming();
        this.liveStateDidChangeListener = DeviceEventEmitter.addListener('lflive.liveStateDidChange', this._liveStateDidChange.bind(this))
    }

    _liveStateDidChange(e) {
        this.setState({
            status: e
        })
        this.props.liveStateDidChangeListener && this.props.liveStateDidChangeListener(e)
    }

    componentWillUnmount() {
        this.liveStateDidChangeListener.remove();
        NativeModules.RNLiveVideoViewManager.stopStreaming()
    }

    render() {
        return <LiveVideoView {...this.props} />
    }

    startStreaming(url, streamName) {
        NativeModules.RNLiveVideoViewManager.startStreaming(url, streamName);
    }

    stopStreaming() {
        NativeModules.RNLiveVideoViewManager.stopStreaming();
    }

    toggleTorch() {
        NativeModules.RNLiveVideoViewManager.toggleTorch();
    }

    flipCamera() {
        NativeModules.RNLiveVideoViewManager.flipCamera();
    }

    setResolution(width, height) {
        NativeModules.RNLiveVideoViewManager.setResolution(width, height);
    }

    setBitrate(bitrate) {
        NativeModules.RNLiveVideoViewManager.setBitrate(bitrate);
    }

    setFps(fps) {
        NativeModules.RNLiveVideoViewManager.setFps(fps);
    }
};


class RNLiveVideo extends Component {

    getInitialState() {
        return {
            showPreview: false
        }
    }
    render() {
        var previewOrButton = (this.state.showPreview) ?
            <LiveVideo style={{ alignSelf: 'stretch', flex: 1 }} liveStateDidChangeListener={(e) => console.log(e)}>
                <TouchableHighlight onPress={() => LiveVideo.startStreaming('rtmp://stream.panda-os.com/pandaLive', 'mp4:0_fdm8zu97')}><Text style={{color: "#fff", margin: 50}}>START Streaming</Text></TouchableHighlight>
                <TouchableHighlight onPress={() => LiveVideo.stopStreaming()}><Text style={{color: "#fff", margin: 50}}>STOP Streaming</Text></TouchableHighlight>
            </LiveVideo> :
            <TouchableHighlight onPress={() => this.setState({showPreview: true})}><Text>Open Streaming</Text></TouchableHighlight>

        return (
            <View style={styles.container}>
                {previewOrButton}
            </View>
        );
    }
};

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});
AppRegistry.registerComponent('LiveVideo', () => LiveVideo);

AppRegistry.registerComponent('RNLiveVideo', () => RNLiveVideo);
