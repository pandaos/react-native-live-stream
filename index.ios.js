/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';

import {LiveVideo} from "./cameraview";

export default class AwesomeApp extends Component {
  render() {
    return (
      <View style={styles.container}>
          <LiveVideo ref="camera" style={styles.livevideo} onConnectionStatusChanged={(e) => console.log(e)}/>
          <View style={styles.cameraOverlay}>
              <TouchableHighlight onPress={() => this.refs.camera.startStreaming('rtmp://stream.panda-os.com/pandaLive', 'mp4:0_fdm8zu97')}><Text style={styles.startbutton}>START Streaming</Text></TouchableHighlight>
              <TouchableHighlight onPress={() => this.refs.camera.stopStreaming()}><Text style={styles.stopButton}>STOP Streaming</Text></TouchableHighlight>
              <TouchableHighlight onPress={() => this.refs.camera.toggleTorch()}><Text style={{color: "#000", margin: 50}}>Torch</Text></TouchableHighlight>
              <TouchableHighlight onPress={() => this.refs.camera.flipCamera()}><Text style={{color: "#000", margin: 50}}>Flip Camera</Text></TouchableHighlight>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  livevideo: {
      alignSelf: 'stretch',
      flex: 1,
  },
    cameraOverlay: {
        position: 'absolute',
        marginLeft: 'auto',
        marginRight: 'auto',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    startbutton: {
        color: "#fff",
        margin: 20,
        backgroundColor: "#990000",
    },
    stopButton: {
        color: "#fff",
        margin: 20,
        backgroundColor: "#aaa",
    }
});

AppRegistry.registerComponent('AwesomeApp', () => AwesomeApp);
