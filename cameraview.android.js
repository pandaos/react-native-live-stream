'use strict';

import React, {Component} from 'react';
import ReactNative, {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    requireNativeComponent,
    NativeModules,
    DeviceEventEmitter,
    TouchableHighlight,
    UIManager,
    findNodeHandle
} from 'react-native';

let ReactCameraViewNative = requireNativeComponent('CameraView', null);

export class LiveVideo extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ReactCameraViewNative ref="camera" {...this.props} ></ReactCameraViewNative>
        )
    }

    startStreaming(url, key) {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs.camera), UIManager.CameraView.Commands.startStreaming, [url + "/" + key]
        );
    }

    stopStreaming() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs.camera), UIManager.CameraView.Commands.stopStreaming, []
        );
    }

    toggleTorch() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs.camera), UIManager.CameraView.Commands.toggleTorch, []
        );
    }

    flipCamera() {
        UIManager.dispatchViewManagerCommand(
            findNodeHandle(this.refs.camera), UIManager.CameraView.Commands.flipCamera, []
        );
    }
}

AppRegistry.registerComponent('CameraView', () => LiveVideo);
AppRegistry.registerComponent('ReactCameraView', () => ReactCameraView);